// Service is an interface that exposes the methods of an implementation whose details have been abstracted away.
package com.zuitt.discussion.services;


import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity deleteUser(Long id);
    ResponseEntity updateUser(Long id, User user);
}
