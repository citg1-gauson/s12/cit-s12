package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully.", HttpStatus.OK);
    }

    public ResponseEntity updateUser(Long id, User User){
        User UserForUpdate = userRepository.findById(id).get();

        UserForUpdate.setUsername(User.getUsername());
        UserForUpdate.setPassword(User.getPassword());

        userRepository.save(UserForUpdate);
        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
    }
}
